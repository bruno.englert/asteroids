package hu.bme.aut.asteroids.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.ParcelUuid;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

public class BluetoothCommunicator {

    private OutputStream outputStream;
    private InputStream inStream;

    public BluetoothCommunicator() throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        init();
    }

    private void init() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        BluetoothAdapter blueAdapter = BluetoothAdapter.getDefaultAdapter();
        if (blueAdapter != null) {
            if (blueAdapter.isEnabled()) {
                Set<BluetoothDevice> bondedDevices = blueAdapter.getBondedDevices();

                for (BluetoothDevice bondedDevice : bondedDevices) {
                    Log.i("============", bondedDevice.getName());
                }

                if(bondedDevices.size() > 0) {
                    Object[] devices = (Object []) bondedDevices.toArray();
                    BluetoothDevice device = (BluetoothDevice) devices[0]; // position
                    ParcelUuid[] uuids = device.getUuids();
                    BluetoothSocket socket;

                    try {
                        socket = device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                        socket.connect();
                    } catch (Exception e) {
                        socket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
                        socket.connect();
                    }

                    outputStream = socket.getOutputStream();
                    inStream = socket.getInputStream();
                } else {
                    Log.e("error", "No appropriate paired devices.");
                }

            } else {
                Log.e("error", "Bluetooth is disabled.");
            }
        }
    }

    public void write(String s) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
            bw.write(s);
        } catch (IOException e) {
            Log.e("error", "bluetooth write error", e);
        }
    }

    public String read() {
        try {
            final int BUFFER_SIZE = 1024;
            byte[] buffer = new byte[BUFFER_SIZE];

            if (inStream.available() > 0) {
                inStream.read(buffer);
                return new String(buffer);
            }
        } catch (IOException e) {
            Log.e("error", "bluetooth read error", e);
        }
        return "";
    }

}

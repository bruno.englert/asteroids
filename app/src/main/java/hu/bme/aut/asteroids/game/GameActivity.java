package hu.bme.aut.asteroids.game;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import hu.bme.aut.asteroids.R;
import hu.bme.aut.asteroids.game.rendering.GameView;

public class GameActivity extends AppCompatActivity {
	private GameView gameView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		gameView = (GameView) findViewById(R.id.gameView);
	}


	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
}
package hu.bme.aut.asteroids.game.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;

import hu.bme.aut.asteroids.R;

public class Background implements Renderable {

    private int width;
    private int height;
    private final BitmapDrawable bitmapDrawable;

    public Background(Context context) {
        Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        bitmapDrawable = new BitmapDrawable(image);
        bitmapDrawable.setTileModeX(Shader.TileMode.MIRROR);
        bitmapDrawable.setTileModeY(Shader.TileMode.MIRROR);
    }

    public void setDim(int width, int height){
        this.width = width;
        this.height = height;
        bitmapDrawable.setBounds(0, 0, width, height);
    }

    @Override
    public void step() {
        //Do nothing
    }

    @Override
    public void setSize(int size) {
        //Do nothing
    }

    @Override
    public void setAngle(double angle) {
        //Do nothing
    }

    @Override
    public void setPos(int x, int y) {
        //Do nothing
    }

    @Override
    public void render(Canvas canvas) {
        if(bitmapDrawable!=null && canvas!=null)
        {
            bitmapDrawable.draw(canvas);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setARGB(150, 150, 100, 100);
            paint.setStrokeWidth(10);
            paint.setAntiAlias(true);
            canvas.drawCircle(width/2, height-260, 220, paint);
        }
    }
}
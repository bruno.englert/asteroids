package hu.bme.aut.asteroids.game.model;

import android.graphics.Path;

/**
 * Created by Bruno Englert on 2016.12.11..
 */
public interface Collide {
    public boolean isCollision(Path path);
}

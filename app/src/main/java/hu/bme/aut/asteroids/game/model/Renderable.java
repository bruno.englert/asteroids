package hu.bme.aut.asteroids.game.model;

import android.graphics.Canvas;

public interface Renderable {
    void step();
    void setSize(int size);
    void setPos(int x, int y);
    void setAngle(double angle);
    void render(Canvas canvas);
}
package hu.bme.aut.asteroids.game.model;

/**
 * Created by Bruno Englert on 2016.12.11..
 */
public class Vector {
    public double x;
    public double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public Vector(Vector vec) {
        this.x = vec.x;
        this.y = vec.y;
    }

    public double abs(){
        return Math.sqrt(x*x+y*y);
    }

    public double angle(){
        double angle = Math.atan2(y,x)%(2*Math.PI);
        return angle*180/Math.PI;
    }

    public double angleRad(){
        double angle = Math.atan2(y,x)%(2*Math.PI);
        return angle;
    }

    public void multiply(double scalar){
        this.x *= scalar;
        this.y *= scalar;
    }
    public void add(Vector vec){
        this.x += vec.x;
        this.y += vec.y;
    }

}

package hu.bme.aut.asteroids.game.model.shape;

import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;

/**
 * Created by Bruno Englert on 2016.12.11..
 */
public class Shape {
    protected Path path;
    protected Paint paint;
    private float angle;
    private float x;
    private float y;
    private float size;

    public Shape(Path path, int color) {
        this.path = path;

        this.paint = new Paint();
        this.paint.setColor(color);
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.paint.setAntiAlias(true);
    }

    public void translate(float dx, float dy){
        this.x = dx;
        this.y = dy;
    }

    public void rotate(float angle){
        this.angle = angle;
    }

    public void scale(float size){
        Matrix mMatrix = new Matrix();
        mMatrix.postScale(size,size);
        path.transform(mMatrix);
    }

    public Paint getPaint() {
        return paint;
    }

    public Path getPath() {
        Path path = new Path(this.path);
        Matrix mMatrix = new Matrix();
        RectF bounds = new RectF();
        path.computeBounds(bounds, true);
        mMatrix.postRotate(this.angle, bounds.centerX(), bounds.centerY());
        mMatrix.postTranslate(this.x, this.y);
        path.transform(mMatrix);

        return path;
    }

}

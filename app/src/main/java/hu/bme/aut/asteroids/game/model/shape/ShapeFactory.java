package hu.bme.aut.asteroids.game.model.shape;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

/**
 * Created by Bruno Englert on 2016.12.11..
 */
public class ShapeFactory {
    public static Shape createShipShape(int color){
        Point a = new Point(0, 0);
        Point b = new Point(10, -10);
        Point c = new Point(0, 20);
        Point d = new Point(-10, -10);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.lineTo(a.x, a.y);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.lineTo(d.x, d.y);
        path.lineTo(a.x, a.y);
        path.close();

        return new Shape(path, color);
    }

    public static Shape createBulletShape(int color){
        Path path = new Path();
        path.addCircle(0, 0, 2, Path.Direction.CW);
        return new Shape(path, color);
    }
}

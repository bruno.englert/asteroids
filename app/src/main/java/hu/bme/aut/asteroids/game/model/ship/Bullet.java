package hu.bme.aut.asteroids.game.model.ship;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Region;

import hu.bme.aut.asteroids.game.model.Collide;
import hu.bme.aut.asteroids.game.model.Renderable;
import hu.bme.aut.asteroids.game.model.Vector;
import hu.bme.aut.asteroids.game.model.shape.Shape;
import hu.bme.aut.asteroids.game.model.shape.ShapeFactory;

/**
 * Created by Bruno Englert on 2016.12.11..
 */
public class Bullet implements Renderable{
    protected int posX = 500;
    protected int posY = 500;
    protected double angle = 0;

    private final double maxSpeed = 25;
    protected Vector speed = new Vector(1,1);

    private int size = 2;
    private Shape shape;

    private int spanLife = 100;
    private int damage = 20;

    public Bullet(Context context,int color) {
        this.shape = ShapeFactory.createBulletShape(color);
        this.shape.scale(size);
    }

    public void lifeDecay(){
        this.spanLife -= 1;
    }

    public boolean isDead(){
        return this.spanLife<=0;
    }

    public int getDamage(){
        return this.damage;
    }

    public Shape getShape() {
        return shape;
    }

    public void setDead() {
        this.spanLife = -1;
    }

    @Override
    public void step() {
        this.posX += Math.cos(this.angle)*maxSpeed;
        this.posY += Math.sin(this.angle)*maxSpeed;

        this.lifeDecay();
        //this.angle = (int)speed.angle()-90;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void setPos(int x, int y) {
        this.posX = x;
        this.posY = y;
        this.shape.translate(x, y);
    }

    @Override
    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public void render(Canvas canvas) {
        shape.translate((float)this.posX, (float)this.posY);

        if( canvas!=null)
        {
            canvas.drawPath(shape.getPath(), shape.getPaint());
        }
    }

}

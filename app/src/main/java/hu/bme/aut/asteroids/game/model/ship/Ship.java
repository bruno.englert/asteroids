package hu.bme.aut.asteroids.game.model.ship;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hu.bme.aut.asteroids.game.model.Collide;
import hu.bme.aut.asteroids.game.model.Renderable;
import hu.bme.aut.asteroids.game.model.Vector;
import hu.bme.aut.asteroids.game.model.shape.Shape;
import hu.bme.aut.asteroids.game.model.shape.ShapeFactory;

public class Ship implements Renderable, Collide {
    protected int posX = 500;
    protected int posY = 500;
    protected double angle = 0;

    private int screenWidth;
    private int screenHeight;

    private final double maxSpeed = 15;
    protected Vector speed = new Vector(0,0);
    protected Vector acceleration = new Vector(0,0);

    private int size = 2;
    private int health = 60;

    private Context context;

    private Shape shape;

    private final List<Bullet> bullets = new ArrayList<>();

    public Ship(Context context,  int color) {
        this.context = context;
        this.shape = ShapeFactory.createShipShape(color);
        this.shape.scale(size);
    }

    @Override
    public void step() {

        preventShipFromLeavingScreen();

        posX += speed.x;
        posY += speed.y;

        Vector newSpeed = new Vector(speed);
        newSpeed.add(acceleration);
        if ( -maxSpeed <= newSpeed.x && newSpeed.x <= maxSpeed)
            speed.x += acceleration.x;
        if(-maxSpeed <= newSpeed.y && newSpeed.y <= maxSpeed)
            speed.y += acceleration.y;

        this.angle = speed.angle()-90;

        synchronized (bullets) {
            Iterator<Bullet> itr = bullets.iterator();
            while (itr.hasNext()){
                Bullet bullet = itr.next();
                bullet.step();
                if (bullet.isDead())
                    itr.remove();
            }
        }
    }

    private void preventShipFromLeavingScreen() {
        if (posX > screenWidth || posX < 0) {
            speed.x = -speed.x;
        }

        if (posY > screenHeight || posY < 0) {
            speed.y = -speed.y;
        }
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void setPos(int x, int y) {
        this.posX = x;
        this.posY = y;
        this.shape.translate(x, y);
    }

    @Override
    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setScreenSize(int width, int height) {
        this.screenWidth = width;
        this.screenHeight = height;
    }

    @Override
    public void render(Canvas canvas) {
        shape.translate((float)posX, (float)posY);
        shape.rotate((float)this.angle);

        if( canvas!=null)
        {
            synchronized (bullets) {
                for (Bullet bullet : bullets) {
                    bullet.render(canvas);
                }
            }
            canvas.drawPath(shape.getPath(), shape.getPaint());
            canvas.drawRect(getHealtBar(), shape.getPaint());

        }
    }

    public Rect getHealtBar(){
        int pos = 30;
        Rect healtBar = new Rect(posX-pos, posY-pos, posX-pos+health, posY-pos+10);
        return healtBar;
    }

    public void setAcceleration(Vector acceleration) {
        acceleration.multiply(0.012); // lower the acceleration
        this.acceleration = acceleration;
    }

    public void fire(){
        Bullet newBullet = new Bullet(context, Color.argb(255,255,0,0));
        newBullet.setPos(posX, posY);
        newBullet.setAngle(speed.angleRad());

        synchronized (bullets) {
            bullets.add(newBullet);
        }
    }
    public List<Bullet> getBullets(){
        return bullets;
    }

    public void damageShip(int damage){
        if ( (this.health - damage) >=0  )
            this.health -= damage;
    }

    @Override
    public boolean isCollision(Path path) {
        Region region1 =  new Region();
        Region region2 =  new Region();
        Region clip = new Region(0, 0, screenWidth, screenHeight);
        region1.setPath(path, clip);
        region2.setPath(shape.getPath(), clip);
        if (region1.op(region2,Region.Op.INTERSECT))
            return true;
        else
            return false;
    }
}


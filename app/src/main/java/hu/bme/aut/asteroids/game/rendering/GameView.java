package hu.bme.aut.asteroids.game.rendering;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import hu.bme.aut.asteroids.bluetooth.BluetoothCommunicator;
import hu.bme.aut.asteroids.game.model.Background;
import hu.bme.aut.asteroids.game.model.Renderable;
import hu.bme.aut.asteroids.game.model.ship.Ship;
import hu.bme.aut.asteroids.game.model.Vector;

public class GameView extends SurfaceView {

    private RenderLoop renderLoop;
    private GestureListener gestureListener;
    private List<Renderable> entitiesToDraw = new ArrayList();
    private List<Ship> players = new ArrayList();

    private final int nb_players = 1;

    private BluetoothCommunicator bluetoothCommunicator;
    private Background background;

    public GameView(Context context) {
        super(context);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        for (int i = 0; i < nb_players; i++) {
            Ship player = new Ship(context, Color.argb(230, 0, 200, 130));
            player.setPos(800, 800);
            entitiesToDraw.add(player);
            players.add(player);
        }

        Ship mainPlayer = new Ship(context, Color.argb(230, 200, 0, 180));
        mainPlayer.setPos(800, 800);
        entitiesToDraw.add(mainPlayer);
        gestureListener = new GestureListener(mainPlayer);
        setOnTouchListener(gestureListener);
        players.add(mainPlayer);

        this.background = new Background(context);
        entitiesToDraw.add(0, background);

        try {
            bluetoothCommunicator = new BluetoothCommunicator();
        } catch (Exception e) {
            e.printStackTrace();
        }

        renderLoop = new RenderLoop(context, this, players, entitiesToDraw);
        SurfaceHolder holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                renderLoop = new RenderLoop(context, GameView.this, players, entitiesToDraw);
                renderLoop.setRunning(true);
                renderLoop.start();
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                renderLoop.setRunning(false);
                while (retry) {
                    try {
                        renderLoop.join();

                        retry = false;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                background.setDim(width, height);
                for (Ship ship : players) {
                    ship.setScreenSize(width, height);
                }

                gestureListener.setSize(width, height);
            }
        });
    }

    public int width() {
        return getWidth();
    }

    private class GestureListener implements OnTouchListener {

        private Ship playerShip;
        private Point center;

        //In this test, handle maximum of 2 pointer
        final int MAX_POINT_CNT = 2;

        boolean[] isTouch = new boolean[MAX_POINT_CNT];

        float[] x = new float[MAX_POINT_CNT];
        float[] y = new float[MAX_POINT_CNT];

        public GestureListener(Ship playerShip) {
            this.playerShip = playerShip;
        }

        public void setSize(int height, int width) {
            this.center = new Point(height / 2, width - 260);
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            int pointerIndex = MotionEventCompat.getActionIndex(motionEvent);
            int pointerId = motionEvent.getPointerId(pointerIndex);
            int action = motionEvent.getActionMasked();
            int pointCnt = motionEvent.getPointerCount();

            if (pointCnt <= MAX_POINT_CNT) {
                if (pointerIndex <= MAX_POINT_CNT - 1) {

                    for (int i = 0; i < pointCnt; i++) {
                        int id = motionEvent.getPointerId(i);
                        x[id] = motionEvent.getX(i);
                        y[id] = motionEvent.getY(i);
                    }

                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            isTouch[pointerId] = true;
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            isTouch[pointerId] = true;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            isTouch[pointerId] = true;
                            break;
                        case MotionEvent.ACTION_UP:
                            isTouch[pointerId] = false;
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            isTouch[pointerId] = false;
                            break;
                        case MotionEvent.ACTION_CANCEL:
                            isTouch[pointerId] = false;
                            break;
                        default:
                            isTouch[pointerId] = false;
                    }
                }
            }


            if (pointCnt == 1) {
                if (isTouch[0]) {
                    Vector vector = deltaCenter(x[0], y[0]);
                    handleTouch(vector);
                } else {
                    playerShip.setAcceleration(new Vector(0, 0));
                }
            }
            if (pointCnt == 2) {
                if (isTouch[0]) {
                    Vector vector = deltaCenter(x[0], y[0]);
                    handleTouch(vector);
                }
                if (isTouch[1]) {
                    Vector vector = deltaCenter(x[1], y[1]);
                    handleTouch(vector);
                }
            }

            return true;
        }

        private Vector deltaCenter(float x, float y) {
            double xComponent = x - this.center.x;
            double yComponent = y - this.center.y;

            Vector vector = new Vector(xComponent, yComponent);
            return vector;
        }

        private void handleTouch(Vector vector) {
            if (260 < vector.abs()) {
                playerShip.fire();
                bluetoothCommunicator.write("fired at " + System.currentTimeMillis());
            }

            if (vector.abs() <= 230) {
                playerShip.setAcceleration(vector);
            }
        }
    }

}

package hu.bme.aut.asteroids.game.rendering;

import android.content.Context;
import android.graphics.Canvas;

import java.util.List;

import hu.bme.aut.asteroids.game.model.Renderable;
import hu.bme.aut.asteroids.game.model.ship.Ship;

public class RenderLoop extends Thread {
  private GameView view;
  private final Renderer renderer;

  private boolean running = false;

  public RenderLoop(Context context, GameView view, List<Ship> players, List<Renderable> entitiesToDraw) {
    this.view = view;
    this.renderer = new Renderer(context, players, entitiesToDraw);
  }

  public void setRunning(boolean run) {
    running = run;
  }

  public static final long FPS = 30;
  private static final long timeBetweenFrames = 1000 / FPS;

  private void sleepThread(long time) {
    try {
      sleep(time);
    } catch (InterruptedException e) {
    }
  }

  private long getTime() {
    return System.currentTimeMillis();
  }

  @Override
  public void run() {
    while (running) {
      long renderStart = getTime();
      draw();

      long renderEnd = getTime();
      long sleepTime = timeBetweenFrames - (renderEnd - renderStart);
      if (sleepTime > 0) {
        sleepThread(sleepTime);
      } else {
        sleepThread(5);
      }
    }
  }

  private void draw() {
    renderer.step();
    Canvas c = null;
    try {
      c = view.getHolder().lockCanvas();
      synchronized (view.getHolder()) {
        renderer.draw(c);
      }
    } finally {
      if (c != null) {
        view.getHolder().unlockCanvasAndPost(c);
      }
    }
  }

}
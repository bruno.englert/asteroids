package hu.bme.aut.asteroids.game.rendering;

import android.content.Context;
import android.graphics.Canvas;

import java.util.List;
import java.util.Random;

import hu.bme.aut.asteroids.game.model.Renderable;
import hu.bme.aut.asteroids.game.model.ship.Bullet;
import hu.bme.aut.asteroids.game.model.ship.Ship;

public class Renderer {
    private Context context;
    private Random random;
    private List<Ship> ships;

    private List<Renderable> entitiesToDraw;


    public Renderer(Context context, List<Ship> ships, List<Renderable> entitiesToDraw) {
        this.context = context;
        this.entitiesToDraw = entitiesToDraw;
        this.ships = ships;
        random = new Random();
    }

    public void step() {
        for (Renderable object : entitiesToDraw) {
            object.step();
        }
        stepBulletCollision();
    }

    public void draw(Canvas canvas) {
        for (Renderable object : entitiesToDraw) {
            object.render(canvas);
        }
    }

    public void stepBulletCollision(){
        int nb_ships = ships.size();
        for (int i = 0; i < nb_ships; i++) {
            for (int k = i+1; k < nb_ships; k++) {
                List<Bullet> bullets = ships.get(k).getBullets();
                synchronized (bullets) {
                    Ship ship = ships.get(i);

                    for (Bullet bullet : bullets) {
                        if (ship.isCollision(bullet.getShape().getPath())) {
                            ship.damageShip(bullet.getDamage());
                            bullet.setDead();
                        }
                    }
                }
            }
        }
    }

}